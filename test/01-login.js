const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const api = chai.request('https://qaplay.dot.co.id/');

describe('Login Test', function(){
    let response = {};
    let token = null;
    let user_id = null;

    it('Should Success login with valid data credentials', function(done){
        let email = 'kris.caden@example.net';
        let password = 'password';
        let username = 'Hyman Zemlak';

        api.post('api/v1/login')
        .set('Content-Type', 'application/json')
        .send({
            email: email,
            password: password
        })
        .end(function(err, res){
            response = res;
            expect(response.status).to.equals(200);
            expect(response.body.data.email).to.equals(email);
            expect(response.body.data.name).to.equals(username);
            token = response.body.data.token;
            user_id = response.body.data.id;
            done();
        });
    });
    
    it('Should Login Failed with invalid data credentials', function(done){
        let email = 'kris.caden@example.net';
        let password = 'wrongpassword';

        api.post('api/v1/login')
        .set('Content-Type', 'application/json')
        .send({
            email: email,
            password: password
        })
        .end(function(err, res){
            response = res;
            expect(response.status).to.equals(422);
            done();
        });
    });

    after(function (done) {
        global.token = token;
        global.user_id = user_id;
        // console.log(global.token);
        // console.log(global.user_id);
        done();
    });
});
